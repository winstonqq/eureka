package com.zhuawa.message.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/msg")
public class MessageController {

    @Value("${server.port}")
    private String port;

    /**
     * 返回一条消息
     */
    @GetMapping("/get")
    public String getMsg() {
        return "This message is sent from port: " + port;
    }

}
