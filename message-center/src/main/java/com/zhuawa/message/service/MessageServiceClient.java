package com.zhuawa.message.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

//@FeignClient(name = "message-service")
@FeignClient(name = "message-service",fallback = MessageServiceFallback.class)
public interface MessageServiceClient {

    @GetMapping("/api/msg/get")
    String getMsg();
}
