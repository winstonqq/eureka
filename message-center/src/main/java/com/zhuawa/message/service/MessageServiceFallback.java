package com.zhuawa.message.service;

import org.springframework.stereotype.Component;

@Component
public class MessageServiceFallback implements MessageServiceClient {

    @Override
    public String getMsg() {
        System.out.println("调用消息接口失败，对其进行降级处理！");
        return "消息接口繁忙，请稍后重试！";
    }
}
