package com.zhuawa.message.controller;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.zhuawa.message.service.MessageServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/center/msg")
public class MessageCenterController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private MessageServiceClient messageService;

    @GetMapping("/get")
    public Object getMsg() {
        String msg = restTemplate.getForObject("http://message-service/api/msg/get", String.class);
        return msg;
    }

    @GetMapping("/feign/get")
    @HystrixCommand(fallbackMethod = "getMsgFallback")
    public Object getMsgFromFeign() {
        String msg = messageService.getMsg();
        return msg;
    }

    public Object getMsgFallback() {
        return "祝您 1024 程序员节 快乐！    *************来自断路器的祝福";
    }
}
