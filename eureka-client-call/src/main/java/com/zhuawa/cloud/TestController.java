package com.zhuawa.cloud;

import com.zhuawa.cloud.service.ConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private ConsumptionService consumptionService;

    @GetMapping("getService")
    public String consumption(String type) {
        return consumptionService.callService(type);
    }
}
